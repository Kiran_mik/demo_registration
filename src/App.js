// import React from 'react';
// import logo from './logo.svg';
// import './App.css';
// import Register from './Register'

// function App() {
//   return (
//     // <div className="App">
//     //   <header className="App-header">
//     //     <img src={logo} className="App-logo" alt="logo" />
//     //     <p>
//     //       Edit <code>src/App.js</code> and save to reload.
//     //     </p>
//     //     <a
//     //       className="App-link"
//     //       href="https://reactjs.org"
//     //       target="_blank"
//     //       rel="noopener noreferrer"
//     //     >
//     //       Learn React
//     //     </a>
//     //   </header>
//     // </div>
//     <Register/>
//   );
// }

// export default App;


import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { store, persistor } from './store';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Register from './Register'
import Login from './Login'
import HomePage from './Homepage'

class App extends Component {

  
  render() {
   
   
    return (
        <Provider store={store}>
          <Router>
            <Switch>
          

              <Route exact path="/" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/homePage" component={HomePage} />


			  


            </Switch>
          </Router>
          </Provider>
    );
  }
}

export default App;
