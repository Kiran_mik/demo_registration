import React, { Component } from 'react';
import SimpleReactValidator from 'simple-react-validator';
import { withRouter } from "react-router-dom";
import * as actions from './actions'
import { connect } from 'react-redux';
class Register extends Component {
    constructor(props){
        super(props)
        this.validator = new SimpleReactValidator();
        this.state = {
            fname:'',
            mname:'',
            lname:'',
            uname:'',
            email:'',
            pswRepeat:'',
            psw:'',
            err:{}

          }
    }

    submit=()=>{
if (this.validator.allValid()) {

let body={
   email: this.state.email,password:this.state.psw
}
this.props.login(body)




    // localStorage.setItem('email',this.state.email)
    // localStorage.setItem('psw',this.state.psw)
this.props.history.push('/login')
  } else {
    this.validator.showMessages();
    this.forceUpdate();
  }
    }

   handleChange=(e)=>{
         this.setState({[e.target.name]:e.target.value})
   }
    
    render() { 
        let {fname,mname,lname,uname,email,psw,pswRepeat}=this.state
        return ( 

<div>
  <div className="container">
    <h1>Register</h1>
<label ><b>First Name</b></label>
    <input type="text" placeholder="Enter First Name" name="fname" value={fname} onChange={this.handleChange}/>
{this.validator.message('fname', this.state.fname, 'required|fname')}
    <label><b>Middle Name</b></label>
    <input type="text" placeholder="Enter middle name" name="mname" value={mname} onChange={this.handleChange}/>
{this.validator.message('mname', this.state.mname, 'required|mname')}
    <label><b>Last Name</b></label>
    <input type="text" placeholder="Enter Last Name" name="lname" value={lname} onChange={this.handleChange}/>
{this.validator.message('lname', this.state.lname, 'required|lname')}
    <label><b>User Name</b></label>
    <input type="text" placeholder="Enter Usernmae" name="uname" value={uname} onChange={this.handleChange}/>
{this.validator.message('uname', this.state.uname, 'required|uname')}
 <label><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" value={email} onChange={this.handleChange}/>
{this.validator.message('email', this.state.email, 'required|email')}
    <label><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" value={psw} onChange={this.handleChange}/>
{this.validator.message('psw', this.state.psw, 'required|psw')}
    <label><b>Repeat Password</b></label>
    <input type="password" placeholder="Repeat Password" name="pswRepeat" value={pswRepeat} onChange={this.handleChange}/>
{this.validator.message('pswRepeat', this.state.pswRepeat, 'required|pswRepeat')}
    <button type="button" className="registerbtn" onClick={()=>this.submit()}>Register</button>
  </div>
  
  </div>
         );
    }
}
 
// export default (null,actions);
export default withRouter( connect(null, actions)(Register))