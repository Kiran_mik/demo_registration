import React, { Component } from 'react';
import { connect } from 'react-redux';
class Login extends Component {
    constructor(props){
        super(props)
this.state = { 
    email:'',
    psw:'',
    getEmail:'',
    getpsw:''
 }
    }
    handleChange=(e)=>{
        this.setState({[e.target.name]:e.target.value})
    }
    
    componentDidMount(){
     if(this.props.uDetails&&this.props.uDetails.userDetails.userDetails){
         var details=this.props.uDetails.userDetails.userDetails
           this.setState({getEmail:details.email,getpsw:details.password})
     }
    }

    login=()=>{
        let {email,getEmail,getpsw,psw}=this.state
        if(email===getEmail&&getpsw===psw){
            this.props.history.push('/homePage')
        }
    }
    render() { 
        let{ email,psw}=this.state
        return ( 
            <div>
           

    <div className="container">
      <label><b>Email</b></label>
      <input type="text" placeholder="Enter Email" name="email" value={email} onChange={this.handleChange} />

      <label ><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="psw" value={psw} onChange={this.handleChange}/>
        
      <button type="button" onClick={()=>this.login()}>Login</button>
    </div>

            </div>
         );
    }
}
 const mapStateToProps = state => ({
	uDetails: state.userDet,
});

// export default Login;
export default connect(mapStateToProps, null)(Login)